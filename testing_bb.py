{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 78,
   "id": "9b0cf596",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atlassian import Bitbucket # Function Oriented\n",
    "\n",
    "import requests\n",
    "import json\n",
    "\n",
    "from rauth import OAuth2Service\n",
    "from atlassian.bitbucket import Cloud # Object Oriented\n",
    "\n",
    "bitbucket_email = \"xxxx@gmail.com\"\n",
    "bitbucket_username = \"iamcowpowder\"\n",
    "bitbucket_app_password = \"hhqjpS732pnyzBnuWqWm\"\n",
    "\n",
    "repo_name = 'test_repo'\n",
    "\n",
    "work_space = 'bitbucket-workspace-test'\n",
    "work_space_id = 'xxxxxx'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adba3738",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 125,
   "id": "190a921a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{\"pagelen\": 10, \"values\": [], \"page\": 1, \"size\": 0}\n"
     ]
    }
   ],
   "source": [
    "# List of Repositories\n",
    "url = \"https://api.bitbucket.org/2.0/repositories/workspace_id_1/\"\n",
    "\n",
    "headers = {\n",
    "   \"Authorization\": \"DxZtQm1vhYf6EhaJhi2A49F6\"\n",
    "}\n",
    "\n",
    "response = requests.request(\n",
    "   \"GET\",\n",
    "   url,\n",
    "   headers=headers\n",
    ")\n",
    "\n",
    "print(response.text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "id": "efd09d52",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{\"pagelen\": 50, \"values\": [{\"description\": \"\", \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/workspaces/workspace_id_1/projects/TES\"}, \"html\": {\"href\": \"https://bitbucket.org/workspace_id_1/workspace/projects/TES\"}, \"repositories\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/workspace_id_1?q=project.key=\\\"TES\\\"\"}, \"avatar\": {\"href\": \"https://bitbucket.org/account/user/workspace_id_1/projects/TES/avatar/32?ts=1647865256\"}}, \"uuid\": \"{abd71187-619d-44e6-a533-fe55c0dfdc86}\", \"created_on\": \"2022-03-21T03:43:39.274578+00:00\", \"workspace\": {\"slug\": \"workspace_id_1\", \"type\": \"workspace\", \"name\": \"workspace_1test\", \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/workspaces/workspace_id_1\"}, \"html\": {\"href\": \"https://bitbucket.org/workspace_id_1/\"}, \"avatar\": {\"href\": \"https://bitbucket.org/workspaces/workspace_id_1/avatar/?ts=1647861855\"}}, \"uuid\": \"{762595a3-de69-41ee-82b2-cba32da69b19}\"}, \"is_private\": false, \"key\": \"TES\", \"owner\": {\"display_name\": \"Desmond Teo\", \"uuid\": \"{762595a3-de69-41ee-82b2-cba32da69b19}\", \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/users/%7B762595a3-de69-41ee-82b2-cba32da69b19%7D\"}, \"html\": {\"href\": \"https://bitbucket.org/%7B762595a3-de69-41ee-82b2-cba32da69b19%7D/\"}, \"avatar\": {\"href\": \"https://secure.gravatar.com/avatar/c1a64d4d73ec3f2247fb60f35380c96d?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FDT-3.png\"}}, \"type\": \"user\", \"nickname\": \"Desmond Teo\", \"account_id\": \"611b25a37b538a00696db2a3\"}, \"updated_on\": \"2022-03-21T12:20:56.349632+00:00\", \"type\": \"project\", \"has_publicly_visible_repos\": false, \"name\": \"test_2\"}, {\"description\": null, \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/workspaces/workspace_id_1/projects/AIM\"}, \"html\": {\"href\": \"https://bitbucket.org/workspace_id_1/workspace/projects/AIM\"}, \"repositories\": {\"href\": \"https://api.bitbucket.org/2.0/repositories/workspace_id_1?q=project.key=\\\"AIM\\\"\"}, \"avatar\": {\"href\": \"https://bitbucket.org/account/user/workspace_id_1/projects/AIM/avatar/32?ts=1646813173\"}}, \"uuid\": \"{cf73fb72-3f09-468c-8e1b-e352f5641915}\", \"created_on\": \"2022-03-09T08:06:13.138748+00:00\", \"workspace\": {\"slug\": \"workspace_id_1\", \"type\": \"workspace\", \"name\": \"workspace_1test\", \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/workspaces/workspace_id_1\"}, \"html\": {\"href\": \"https://bitbucket.org/workspace_id_1/\"}, \"avatar\": {\"href\": \"https://bitbucket.org/workspaces/workspace_id_1/avatar/?ts=1647861855\"}}, \"uuid\": \"{762595a3-de69-41ee-82b2-cba32da69b19}\"}, \"is_private\": false, \"key\": \"AIM\", \"owner\": {\"display_name\": \"Desmond Teo\", \"uuid\": \"{762595a3-de69-41ee-82b2-cba32da69b19}\", \"links\": {\"self\": {\"href\": \"https://api.bitbucket.org/2.0/users/%7B762595a3-de69-41ee-82b2-cba32da69b19%7D\"}, \"html\": {\"href\": \"https://bitbucket.org/%7B762595a3-de69-41ee-82b2-cba32da69b19%7D/\"}, \"avatar\": {\"href\": \"https://secure.gravatar.com/avatar/c1a64d4d73ec3f2247fb60f35380c96d?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FDT-3.png\"}}, \"type\": \"user\", \"nickname\": \"Desmond Teo\", \"account_id\": \"611b25a37b538a00696db2a3\"}, \"updated_on\": \"2022-03-09T08:06:13.138767+00:00\", \"type\": \"project\", \"has_publicly_visible_repos\": false, \"name\": \"aimltest\"}], \"page\": 1, \"size\": 2}\n"
     ]
    }
   ],
   "source": [
    "# list of Projects\n",
    "url = \"https://api.bitbucket.org/2.0/workspaces/workspace_id_1/projects/\"\n",
    "\n",
    "headers = {\n",
    "   #\"Authorization\": \"Bearer HL6FkYchgAmH2bpevsMGC733\"\n",
    "   \"Authorization\": \"DxZtQm1vhYf6EhaJhi2A49F6\"\n",
    "}\n",
    "\n",
    "response = requests.request(\n",
    "   \"GET\",\n",
    "   url,\n",
    "   headers=headers\n",
    ")\n",
    "\n",
    "print(response.text)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "id": "aa519d89",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{\n",
      "    \"error\": {\n",
      "        \"detail\": \"There is no API hosted at this URL.\\n\\nFor information about our API's, please refer to the documentation at: https://developer.atlassian.com/bitbucket/api/2/reference/\",\n",
      "        \"message\": \"Resource not found\"\n",
      "    },\n",
      "    \"type\": \"error\"\n",
      "}\n"
     ]
    }
   ],
   "source": [
    "# list of Projects\n",
    "url = \"https://api.bitbucket.org/2.0/workspace_id_1/repo_test_1/pull-requests/\"\n",
    "\n",
    "headers = {\n",
    "   #\"Authorization\": \"Bearer HL6FkYchgAmH2bpevsMGC733\"\n",
    "   \"Authorization\": \"DxZtQm1vhYf6EhaJhi2A49F6\"\n",
    "}\n",
    "\n",
    "response = requests.request(\n",
    "   \"GET\",\n",
    "   url,\n",
    "   headers=headers\n",
    ")\n",
    "\n",
    "print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(\",\", \": \")))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 155,
   "id": "a6517865",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<generator object BitbucketBase._get_paged at 0x7fd03501b9e0>"
      ]
     },
     "execution_count": 155,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bitbucket = Bitbucket(\n",
    "    url='https://api.bitbucket.org/2.0/repositories/workspace_id_1',\n",
    "    username='cowpowder',\n",
    "    password='Paosiduf4!')\n",
    "\n",
    "cloud = Cloud(\n",
    "    url=\"https://api.bitbucket.org/2.0/repositories/workspace_id_1\", \n",
    "    username=\"iamcowpowder@gmail.com\", \n",
    "    password=\"Paosiduf4!\",\n",
    "    cloud=True\n",
    "    )\n",
    "#H8q8aZkPuQfqykCDFk\n",
    "bitbucket_app_pw = Cloud(\n",
    "    username=\"cowpowder\",\n",
    "    password=\"hhqjpS732pnyzBnuWqWm\",\n",
    "    cloud=True)\n",
    "\n",
    "project_key = 'AIM'\n",
    "repository_slug = 'repo_test_4'\n",
    "\n",
    "import requests\n",
    "import json\n",
    "\n",
    "\n",
    "headers = {\n",
    "    'H8q8aZkPuQfqykCDFk': 'QZrSq5pbZVnZVfQGa4JHHJd42ryUw4Ca',\n",
    "    'Content-Type': 'application/json'\n",
    "}\n",
    "\n",
    "bitbucket.project_list()\n",
    "\n",
    "#Get repo list\n",
    "\n",
    "# url = \"https://api.bitbucket.org/2.0/repositories/workspace_id_1\"\n",
    "# response = requests.request(\"GET\", url, headers=headers)\n",
    "\n",
    "# print(response) \n",
    "# print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(\",\", \": \")))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 116,
   "id": "d33a2db0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Name: numpy\n",
      "Version: 1.20.3\n",
      "Summary: NumPy is the fundamental package for array computing with Python.\n",
      "Home-page: https://www.numpy.org\n",
      "Author: Travis E. Oliphant et al.\n",
      "Author-email: \n",
      "License: BSD\n",
      "Location: /Users/desmondteoyihao/opt/anaconda3/lib/python3.9/site-packages\n",
      "Requires: \n",
      "Required-by: tifffile, tables, statsmodels, seaborn, scipy, scikit-learn, scikit-image, PyWavelets, pyerfa, patsy, pandas, numexpr, numba, mkl-random, mkl-fft, matplotlib, imageio, imagecodecs, h5py, dataiku-internal-client, daal4py, Bottleneck, bokeh, bkcharts, astropy\n",
      "Note: you may need to restart the kernel to use updated packages.\n"
     ]
    }
   ],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28efa6cd",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
