{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Advanced Model Insights for Regression\n",
    "\n",
    "This notebook explores additional options for model insights added in the v2.18 release of the DataRobot API that apply specifically to regression models.\n",
    "\n",
    "## Prerequisites\n",
    "In order to run this notebook yourself, you will need the following:\n",
    "\n",
    "- This notebook. If you are viewing this in the HTML documentation bundle, you can download all of the example notebooks and supporting materials from [Downloads](../index.rst).\n",
    "- A DataRobot API token. You can find your API token by logging into the DataRobot Web User Interface and looking in your `Profile`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparation\n",
    "\n",
    "This notebook explores additional options for model insights added in the v2.7 release of the DataRobot API.\n",
    "\n",
    "Let's start with importing some packages that will help us with presentation (if you don't have them installed already, you'll have to install them to run this notebook)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "import datarobot as dr\n",
    "import numpy as np\n",
    "from datarobot.enums import AUTOPILOT_MODE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configure the Python Client\n",
    "Configuring the client requires the following two things:\n",
    "\n",
    "- A DataRobot endpoint - where the API server can be found\n",
    "- A DataRobot API token - a token the server uses to identify and validate the user making API requests\n",
    "\n",
    "The endpoint is usually the URL you would use to log into the DataRobot Web User Interface (e.g., https://app.datarobot.com) with \"/api/v2/\" appended, e.g., (https://app.datarobot.com/api/v2/).\n",
    "\n",
    "You can find your API token by logging into the DataRobot Web User Interface and looking in your `Profile.`\n",
    "\n",
    "The Python client can be configured in several ways. The example we'll use in this notebook is to point to a `yaml` file that has the information. This is a text file containing two lines like this:\n",
    "```yaml\n",
    "endpoint: https://app.datarobot.com/api/v2/\n",
    "token: not-my-real-token\n",
    "```\n",
    "\n",
    "If you want to run this notebook without changes, please save your configuration in a file located under your home directory called `~/.config/datarobot/drconfig.yaml`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<datarobot.rest.RESTClientObject at 0x108af23c8>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Initialization with arguments\n",
    "# dr.Client(token='<API TOKEN>', endpoint='https://<YOUR ENDPOINT>/api/v2/')\n",
    "\n",
    "# Initialization with a config file in the same directory as this notebook\n",
    "# dr.Client(config_path='drconfig.yaml')\n",
    "\n",
    "# Initialization with a config file located at\n",
    "# ~/.config/datarobot/dr.config.yaml\n",
    "dr.Client()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Project with features\n",
    "\n",
    "Create a new project using the 10K_diabetes dataset. This dataset contains a binary classification on the target `readmitted`.  This project is an excellent example of the advanced model insights available from DataRobot models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Project ID: 5dee769c708e5938ec312aa0\n"
     ]
    }
   ],
   "source": [
    "url = 'https://s3.amazonaws.com/datarobot_public_datasets/NCAAB2009_20.csv'\n",
    "project = dr.Project.create(\n",
    "    url, project_name=\"NCAA Men's Basketball 2008-09 season\"\n",
    ")\n",
    "print('Project ID: {}'.format(project.id))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Project(NCAA Men's Basketball 2008-09 season)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Increase the worker count to your maximum available the project runs faster.\n",
    "project.set_worker_count(-1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Project(NCAA Men's Basketball 2008-09 season)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "target_feature_name = 'score_delta'\n",
    "project.set_target(target_feature_name, mode=AUTOPILOT_MODE.QUICK)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "In progress: 4, queued: 6 (waited: 0s)\n",
      "In progress: 4, queued: 6 (waited: 0s)\n",
      "In progress: 4, queued: 6 (waited: 1s)\n",
      "In progress: 3, queued: 6 (waited: 1s)\n",
      "In progress: 2, queued: 5 (waited: 2s)\n",
      "In progress: 4, queued: 3 (waited: 4s)\n",
      "In progress: 4, queued: 3 (waited: 7s)\n",
      "In progress: 2, queued: 0 (waited: 14s)\n",
      "In progress: 1, queued: 0 (waited: 27s)\n",
      "In progress: 1, queued: 0 (waited: 47s)\n",
      "In progress: 4, queued: 12 (waited: 67s)\n",
      "In progress: 4, queued: 11 (waited: 87s)\n",
      "In progress: 4, queued: 3 (waited: 107s)\n",
      "In progress: 2, queued: 0 (waited: 128s)\n",
      "In progress: 1, queued: 0 (waited: 148s)\n",
      "In progress: 4, queued: 1 (waited: 168s)\n",
      "In progress: 0, queued: 0 (waited: 188s)\n",
      "In progress: 0, queued: 0 (waited: 208s)\n"
     ]
    }
   ],
   "source": [
    "project.wait_for_autopilot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Model('TensorFlow Neural Network Regressor')"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "models = project.get_models()\n",
    "model = models[0]\n",
    "model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's set some color constants to replicate visual style of the DataRobot residuals chart."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "dr_dark_blue = '#08233F'\n",
    "dr_blue = '#1F77B4'\n",
    "dr_orange = '#FF7F0E'\n",
    "dr_red = '#BE3C28'\n",
    "dr_light_blue = '#3CA3E8'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Residuals Chart\n",
    "\n",
    "The residuals chart is only available for non-time aware regression models. It provides a scatter plot showing how predicted values relate to actual values across the data. For large data sets, the value is downsampled to a maximum of 1,000 data points per data source (validation, cross validation, and holdout).\n",
    "\n",
    "The residuals chart also offers the residual mean (arithmetic mean of predicted values minus actual values) and coefficient of determination, also known as the r-squared value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "residuals = model.get_all_residuals_charts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ResidualChart(holdout), ResidualChart(validation), ResidualChart(crossValidation)]\n"
     ]
    }
   ],
   "source": [
    "print(residuals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you see, there are three charts for this model corresponding to the three data sources. Let's look at the validation data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Coefficient of determination: 0.009472645884915032\n",
      "Residual mean: 0.2240474092818442\n"
     ]
    }
   ],
   "source": [
    "validation = residuals[1]\n",
    "print('Coefficient of determination:', validation.coefficient_of_determination)\n",
    "print('Residual mean:', validation.residual_mean)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0.5,1.04,'Predicted Values vs. Actual Values')"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYoAAAEfCAYAAABf1YHgAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3XmcJHV9//HXe3p2Z3aXheXYCxbYBTYaLoHdgMbjRwQUvBbjkSUekENEzUNj4oHBKCaaKIka88uBBDWoqCEeAaOISDhijCgIyCnXrj8WdllA9oBlj5n5/P6o6tmanu7q7pnurp7p9/PxmN3uqp6qT3f11Lu+37oUEZiZmdXSV3QBZmbW3RwUZmaWy0FhZma5HBRmZpbLQWFmZrkcFGZmlstBYbkkLZUUkvrT51dKOrMD8z1f0pen2rR7UeV3ZKpM2xrnoJgGJK2V9IykpyQ9KulfJe3RjnlFxGkRcUmDNZ3c6vlLOkDSkKRDq4z7lqS/bfU8u126vIckLW7id0LSYe2sK53P9yT9RZXhqyRtcABMDQ6K6eOVEbEHcBywEvhg5QuUmNLLPCIeBq4B3pQdLmkf4GVA3RCbTiTNAV4DbAbeWHA51VwCvFGSKoa/Cbg0IoYKqMmaNKVXGjZeuiK9EjgSQNJ1kj4m6X+AbcAhkvaS9DlJ6yU9LOmjkkrp60uS/lbS45IeBF6enX46vT/MPH+LpLslbZV0l6TjJH0JOAj4dtrKeV/62udK+pGkTZJuk3RiZjrLJF2fTudqYL+ct3kJFUEBrAbuiojb0+l9RtJDkrZIulnSC6tNSNKJktZVDBttDUnqk3SupAckPSHpsjSUkDQo6cvp8E2SfippYZV5vF/S1yuGfUbS36ePz5L0YPre10h6Q857r/QaYBPwF8CYLsF0Wf5ZWvvW9HM4UNIN6UtuS5fP76Q1/LDi90dbHZJeLumW9PN8SNL5Ddb3H8C+wOjnL2lv4BXAF5uddmVLVRXdiHW+Y5P5nHtbRPhniv8Aa4GT08cHAncCf5k+vw74f8ARQD8wA/gW8FlgDrAA+Anw1vT15wD3pNPZB7gWCKA/M70/TB+/DngY+A1AwGHAwZU1pc8PAJ4g2ervA05Jn89Px/8v8ClgAHgRsBX4co33O4tkC/oFmWH/C/xx5vkbSVZQ/cCfAhuAwXTc+eVpAycC63I+z3cBPwaWpLV9FvhqOu6twLeB2UAJWAHsWaXeg0lCem76vASsB56bLoMtwLPScYuBI5pY9tcAFwALgSFgRWbce4HbgWely+c5wL7puAAOy7z2LOCHFdMefU36OR2VLrujgUeB09NxS7PfkSo1/gtwceb5W4FbM88bnnaV71V2Wdb8jk32c+71n8IL8E8LFmLyx/MUyZblL4F/Amal464D/iLz2oXAjvL4dNgZwLXp4/8CzsmMewm1g+Iq4F05NWX/oN8PfKniNVeRbAUflK7k5mTGfYUaQZGOvxi4KH28HNgJLMh5/ZPAc9LH2ZXLieQHxd3ASZlxi4FdJAH0+8CPgKMbWEY/BN6cPj4FeCB9PCddbq/JLpMGl/tBwAhwTObz/Exm/C+AVTV+t6mgqPL7fwd8On28lPygeEH6HstB/T/Au3PeV81pV/leZZdl3ndswp+zf8JdT9PI6RExLyIOjoi3R8QzmXEPZR4fTNKqWJ82zzeRbCUvSMfvX/H6X+bM80DggQbrOxh4XXme6XxfQLLi3R94MiKebnC+kHQ/vU7SIEk31FURsbE8UtJ70i6xzem89iK/Oyuv7m9lar4bGCYJ3C+RrIi+JukRSRdImlFjOl8hCWSA302fk77n3yFpya2X9B1Jz26wtjcBd0fErenzS4HfzdTQzPLJJekESddKekzS5rTehj7PiPgh8DhwupKDEI4nff+TnXaFmt+xSX7OPc9B0Ruylwh+iKRFsV8aLPMiYs+IOCIdv55kBVN2UM50HwLGHX1UZZ7l134pM895ETEnIj6eznNvJTtmG5kvJFvovwJWkXQzje7ETvdHvA94PbB3RMwj6aqq3KEK8DRJ11H5d0skXRXZuk+rqHswIh6OiF0R8ZGIOBz4TZJ+9zfXqPffgRMlLQFeTWZFGRFXRcQpJKF5D0lXTSPeTLLPaYOkDSRdd/uRdL2Ua6+1fCpVfg6LKsZ/BbgCODAi9gIupPrnWcsX03rfSBLqj05w2mPqBLJ15n3HJvM59zwHRY+JiPXA94FPStoz3Vl7qKT/k77kMuCdkpakOx3PzZncxcB7JK1Q4jBJB6fjHgUOybz2y8ArJb003ck6mO5IXhIRvwRuAj4iaaakFwCvrPM+gmTl8wlgHsm+grK5JF1ZjwH9kj4E7FljUvcCg+kO1RkkR4sNZMZfCHys/L4kzZe0Kn38W5KOSsNlC0mX1EiNeh8j6bb7ArAmIu5Op7FQyaGic0gC/Kla08iS9DySEDgeOCb9OZJkpVsOq4uBv5S0PF0+R0vaNx1XuXxuA46QdEzaSju/YpZzgV9FxHZJx5O0iprxReBk4C2MPzKtmWnfCqyWNEPSSuC1mXE1v2MT/ZwtVXTfl38m/0NFv23FuOtI9ylkhu0F/DOwjmRL+xZgdTquH/g0yU7ANcA7qLGPIn1+Dklf+FPAHcCx6fBVJDvRNwHvSYedAFxP0hJ4DPgOcFA67hDgv9PpXA38Azn7KNLfWUbyx/7PFcNLwOdJVt7rSVoXo58RmX7t9PlZ6es2Au+peG0f8Cfpe9xK0pXzV+m4M9LhT5OseP+eGv306evflH6W780MW5x+JpvTz+o64PB03AuBp2pM60LgG1WGH0+yItwn/Rw+mC7HrcBPgSWZ5bY+nefr02HnkXQRPUSy5Z/dmf1aku7ArcB/ZpcPdfZRVHwXnwQGKoY3PO30e3Jj+j35TvqZZ5dl1e9Y3ufsn/o/Sj9cMzOzqtz1ZGZmuRwUZmaWy0FhZma5HBRmZpbLQWFmZrkcFGZmlstBYWZmuRwUZmaWy0FhZma5HBRmZpbLQWFmZrkcFGZmlstBYWZmufqLLqAVVJoRmjFYdBlmZgzOnMG8PQbZNTTMr7Y8Q4y7h1f3iB1PPR4R8+u9bnoExYxBBpasKLoMM+txzzpoPqteeDiPPL6Vy665jZl7DhddUq7tD1xf75bDgLuezMxaojIkdg51d0g0w0FhZjZJ0zkkwEFhZjYp0z0kwEFhZjZhvRAS4KAwM5uQXgkJcFCYmTWtl0ICHBRmZk3ptZAAB4WZWcN6MSTAQWFm1pBeDQlwUJiZ1dXLIQEFBoWkQUk/kXSbpDslfSQdvkzSjZLul/RvkmYWVaOZWa+HBBTbotgBvDgingMcA5wq6bnAJ4BPR8RhwJPAHxRYo5n1MIdEorCgiMRT6dMZ6U8ALwa+ng6/BDi9gPLMrMc5JHYrdB+FpJKkW4GNwNXAA8CmiBhKX7IOOKDG754t6SZJN8Xwrs4UbGY9wSExVqFBERHDEXEMsAQ4Hnh2E797UUSsjIiVKs1oW41m1lscEuN1xVFPEbEJuBZ4HjBPUvk+GUuAhwsrzMx6ikOiuiKPepovaV76eBZwCnA3SWC8Nn3ZmcDlxVRoZr3EIVFbkXe4WwxcIqlEEliXRcR/SroL+JqkjwK3AJ8rsEYz6wEOiXyFBUVE/Bw4tsrwB0n2V5iZtZ1Dor6u2EdhZlYEh0RjHBRm1pMcEo1zUJhZz3FINMdBYWY9xSHRPAeFmfUMh8TEOCjMrCc4JCbOQWFm055DYnIcFGY2rTkkJs9BYWbTlkOiNRwUZjYtOSRax0FhZtOOQ6K1HBRmNq04JFrPQWFm04ZDoj0cFGY2LTgk2sdBYWZTnkOivRwUZjalOSTaz0FhZlOWQ6IzHBRmNiU5JDrHQWFmU45DorMcFGY2pTgkOs9BYWZThkOiGA4KM5sSHBLFcVCYWddzSBTLQWFmXc0hUTwHhZl1LYdEd3BQmFlXckh0DweFmXUdh0R3cVCYWVdxSHSfwoJC0oGSrpV0l6Q7Jb0rHb6PpKsl3Zf+v3dRNZpZZzkkulORLYoh4E8j4nDgucA7JB0OnAtcExHLgWvS52Y2zTkkuldhQRER6yPiZ+njrcDdwAHAKuCS9GWXAKcXU6GZdYpDorv1F10AgKSlwLHAjcDCiFifjtoALKzxO2cDZwPQP9D2Gs2sPRwS3a/wndmS9gC+AfxxRGzJjouIAKLa70XERRGxMiJWqjSjA5WaWas5JKaGQoNC0gySkLg0Ir6ZDn5U0uJ0/GJgY1H1mVn7OCSmjiKPehLwOeDuiPhUZtQVwJnp4zOByztdm5m1l0NiailyH8XzgTcBt0u6NR32Z8DHgcsk/QHwS+D1BdVnZm3gkJh6CguKiPghoBqjT+pkLWbWGQ6Jqanwndlm1hscElOXg8LM2s4hMbU5KMysrRwSU5+DwszaxiExPTgozKwtHBLTh4PCzFrOITG9OCjMrKUcEtOPg8LMWsYhMT05KMysJRwS05eDwswmzSExvTkozGxSHBLTn4PCzCbMIdEbHBRmNiEOid7RUFBIOljSyenjWZLmtrcsM+tmDoneUjcoJL0F+Drw2XTQEuA/2lmUmXUvh0TvaaRF8Q6SmwxtAYiI+4AF7SzKzLqTQ6I3NRIUOyJiZ/mJpH4g2leSmXUjh0TvaiQorpf0Z8AsSacA/w58u71lmVk3cUj0tkaC4lzgMeB24K3Ad4EPtrMoM+seDgmre8/siBgB/iX9MbMe4pAwaCAoJK2hyj6JiDikLRWZWVdwSFhZ3aAAVmYeDwKvA/ZpTzlm1g0cEpZVdx9FRDyR+Xk4Iv4OeHkHajOzAjgkrFIjXU/HZZ72kbQwGmmJmNkU45CwahpZ4X8y83gIWAu8vi3VmFlhHBJWSyNHPf1WJwoxs+I4JCxPzaCQ9Cd5vxgRn2p9OWbWaQ4JqyevReErxJpNcw4Ja0TNoIiIj7R75pI+D7wC2BgRR6bD9gH+DVhKuj8kIp5sdy1mvcYhYY1q5DLjg5LeIemfJH2+/NOi+f8rcGrFsHOBayJiOXBN+tzMWsghYc1o5FpPXwIWAS8Frie5H8XWVsw8Im4AflUxeBVwSfr4EuD0VszLzBIOCWtWI0FxWET8OfB0RFxCcrLdCW2saWFErE8fbwAWVnuRpLMl3STpphje1cZyzKYPh4RNRCNBUV4Lb5J0JLAXHbpxUUQENe59EREXRcTKiFip0oxOlGM2pbUiJPpnz2X2/ofSP9vHuvSSRk64u0jS3iSXFr8C2AP48zbW9KikxRGxXtJiYGMb52XWE1oREnsf+ZssXXUOMTyESv2svfxCnrzjR22o1rpNzRaFpEUAEXFxRDwZETdExCERsSAiPlvr91rgCuDM9PGZwOVtnJfZtNeqlsTSVedQmjFA/+AcSjMGWLrqnEJaFm7VdF5ei+JWSXcAXwW+ERGbWj1zSV8FTgT2k7QO+DDwceAySX8A/BJfLsRswlq1T2LmvAXE8BDMGBgdFsPDzJy3gKFtLTm2pSFu1RQjLygOAE4GVgN/JenHJKFxeUQ804qZR8QZNUad1Irpm5X1z57LzHkL2LlpY0dXbEVq5Y7rnZs2otLY1YVKJXZu6lzPcLZVUw6spavOYeuDt/fMMi1K3gl3w8BVwFWSZgKnkYTG30m6JiLe0KEazSal2lbo1gdv7+rgmGywtfropqFtW1l7+YXp5ziMSiXWXn5hRz+7bmnV9KKGLhceETsl3QXcDawAfr2tVZk1oJGVabWt0GWnv52Ika7tvphs90q7DoF98o4fFRqw3dCq6VW5QSHpQJJWxBnAHJKup1dFxD0dqM2spkZXptW2QlXqp0/qyu6LyXavtPs8iaFtWwv7nLqhVdOr8q4e+yOS/RSXAW+JiJs7VpVZjmZWpjs3baSvf2bu9IrqvqjWIppM90qrjm7q5i65ols1vSqvRXEu8N/pSW9mXaPVfdVFdF/UahFNtHull86TKLJV06tqnkeRnjfhkLCu08zKdOa8BYwM7RwzLIaHiIjRn8duvqbhFU8rjuHPOyeh3L0yvGsHQ9u3MbxrR93ulXoh0UjN3XSeRDv5HIyJ8b2vbcpppK+63IUyvGNblVDpR9Lo8/krTmLDDd+sGxat2uKu1yJqpnulXkiM1jwygvr6mtqXM9kuuW7rxpoqLaZu5KDoId32hzsZeSvTyhXCYzdfw/wVJyWh0t8PI0FpZnMrxFYew9+qo3caaUksO/3t9PXvvhbastPfXnNfTiM1Zb9DQM3vU72Vcqe/iz4HY3J8K9QeMR23pqr1VVdbIcxfcRJ3Xfh+SgOzGd6xjcPP+cSY36m3ku6fPZc9lx8Lw8OQuf7kRLe4a7WIAGbvfyizFx3MgaedlbusGtknMWvRsqqtqVmLlrH1wZ83VFOtAC4fIDAytHNcjfVWykV8F6fLORhFbew1civUZwG/QXINJoBXAj9pZ1HWWr20NVVrhVAamM22Rx4AaOoQyzErx4FZY8ZNZid4ZYto7iFHcdS7/5EYHqI0MDvpGquxrBrdcV0aGGxqeF4rrdp3CBhtrWRrzFspl1/b6e/idDgHo8iNvbq3QpV0A3BcRGxNn58PfKcj1VlLTJetqUY0skJodB9AtZVjRDCy4xmoaAVMZAuv3CKqtRIeFYwuq+aObqp1rErtuwvUOqKo2ndoTImZ71PeMijquzjVz8EoemOvkX0UC4HsYSM7qXEzIetOk92a6mRzd7LzanSFkHeIZbmG0uCccSu14R3P8P+u/AJb7rtlTCtgIlt4efPJ6psxEySOes6RnHbkfjzy+JYGD4EdaXJ4bfXOR8l+n+otg6K27KfyORhFb+w1EhRfBH4i6Vvp89PZfatSmwImszXVyeZuq+Y1mRXCmK6mUj9o7Na3SiW23HcLMLkulHrzqfSi15/J0Y9fy+aZ8/n++p1Vd1xXvt/hHdtrTK1vwq2grMrWVXZatZZB0Vv2U/UcjKK7zuoGRUR8TNKVwAvTQb8XEbe0tyxrtYmsPDvZ3G31vCayQqhWw8jQLoZ37Ri3Upu9/6ET3sLLnU96RFb28N0FTz3IUY9fy5bBBdy6+OUcsCjY/PAaSgOzR/dvVAvYZzasSYZljnqKkWEO+e23M9JkGJfPR8keQZVtXVV7z7WWwVTesi9K0QHb6OGxs4EtEfEFSfMlLYuINe0szFqv2ZXnzHkLxt+INtNf3kqdblo3evmMkaFdPHDZpxjevm3MayezhZc3n9LAHA59/R+PDl/w1IMcufEHbBlcwC2LX85w30wY2sERb7uAkaFdo62Rvv4ZVQN2zX/8U3oeRaA+IfXRN2OAvibDuFrXU1//jJohUc9U3bIvUpEBWzcoJH0YWEly9NMXSA4Q/DLw/PaWZkUb3rEt6R/P6Jsxk+Ed21o+r042rZu9fMYzG9ZW3ceRd4hr3h9yrZXuMxvWJlvuu3ZSmjmQhMSjV7NlIBMSJMtA0ug0Ki+gUHny3rb1a5izZDkjO7ezdNU5Y1oF9cJ4YN/FzFmynO2PP1LvY23KZPZFTafzgarJe39FBWwjLYpXA8cCPwOIiEck+fz3HlAamM3Irh2UZu4+nHJk105KA7NbPq9ONa3rdXGtvfxClq06hwiQyK0huxJ+et19zF68LHfndvZs8WpKs/agNDgHid0hMbiAm+e/hKHhIHZuq3rCYKVswFaGoqrscxnesW003GD3SXSLXvQaFp5w6uhrY2TsfpEYGRkTMo2uwBvZF1VrWoUeItqBgOrW850aCYqdERGSAkDSnDbXZF1i56aNydoyS7RtB1onmtb1urjmLPk11D+T8rues2R5zT/UystjKKcLaMxrS6Wq0zv8bRcQQ7tYsO2XHPn4tWyeuR8373cKD1xxMTue3Mhehx3DU+vu5bDV7xn7iyMjRN/uAChfu6rWvpCRXTsYScP4sZuv4fBzPjHuJLq+Un/yOWSXf9/YurOty0ZvDtXIvqhaK8siDxHtxAq86ENgc2tr4DWXSfosME/SW4DfBy5ub1nWDSa6lV/urnh63X3seGJ97msrt9JKs/ZgcP4BDO/Y1tBO4WZDpVb3UmlwNrMPOIyFJ5w6ZuW48ITTeOyn3x/3PqpdHqNWFxBQ97Xle2Qs2L4uDYn5/PsNd7Pl4StZ9KLXcMhr3jn62m0b1jJ70dLML2vszu+Vp7Dhhm/m7Av5NMPbnx49S73WSXT1rglabl3WujkUMTJup3mtoJ61aCnD27cxvGNbzZVlo/uxWr3l36kVeNGHwOZp5Kinv5V0CrCFZD/FhyLi6rZXNo1M5T7VZrfyl5x61pjuikdvvJJ136t+NHXlVtrWtXex12HHNPe7dS52V5ZdBo/d/F9jauzr6+fQ17+75nkCex5yNE+nRxiV33+1y2NUKncBNfJaGNvddNPeL2b70zdRmrXHuPCavWjpmOeVq/PkMh1LeWbD2hr7XNbUPHKrKWnrstbNoSSN22leLaj7+mdw2Bnv3X2E1kj1wG1kP1Y7tvw7tQIv+hDYPI3szP5ERLwfuLrKMKtj7yN/M+nzTldoa7qkz7EZje5AG9h3cVNb5JVbaXsddsyEt+ZrXewOqp+zMKZLpVSiv5T0qFbbij7wJW9gZFwgVd/azv7+7suX179af+XRTSPDwc5NG5NrTE1AaWDO2BZhetRTdod7tSvr5r2fHZseY2De/Crvb/xJdOOmk65Ytz3ywJhWal+pBOobd/Z7Vl//jNGQzjuAIK81MpkVeqdW4EUfApunka6nU4DKUDityjCr0OwKrVs12iKas2R5zeGVK/t6l4TI+91mLnZX6zIctcTwEGSmHTGSHE6aPi8vv52bH6/6+9UuX17rtSO7djIyPMSiHQ9x1GPXsmVwIT9b9DKGNYPHbr6SoW1beXrdfTVrzbf77Gul/wiYs+TXcltxqtgnlX0+MG9+1S6uaoEk9Y05fyO7Ys22UkuDyeHA2b+RPHnXyOrrn0GMjD3rvBVb/p1cgXfrOSZ5V499G/B24FBJ2b++ucDU2iRuUKu7iGYtWlpjhbaUrQ/ePunpV2pHF1czTflaK7Xs8Lz7RFRT/bDMWiv68cMbDaQxUxnaSUSgvlLNQBre/vS4k9nGTSddSZUGqx//sfY7n2PZ3BGOPqiPzbMWcuv+L2ekL9mRXl4J73hiPY/eeCULTzht93RjBKn6DvGy4R3bq26ojLb4arTimlH5XU6mEgix8aard1/avcqKNXudq3rfg5GhXWNW9nnXyBq/76c1W/6dXIF34zkmeUvoK8CVwF+T3Ba1bGtE/KqtVRWgPUc11PoDnNgfZp521N/sTrxqK7VHb7xytEVQ7z4RfX39Y4+yGhmBzB/+aBBufnz8GcfDQzyzYe24mqp1G8TwECMxknR9pNNITl5LukH6sn3RVVofg/su4ulHHmxiH8XSquOP/62TkstyDMxPupv6ZmZ+t595RzyPTXf+L+u+dwm/uv1/2OuwY9ix+QkOOvVN9NUIn9H3vfmxhveNZEVEculwlBuCu2n0e5L93OavOIlf/OtfMLjf/rkHNVTbWu/r64fMkWHlrqdK1fcdjG0RNnP3wnratQKfCvsw864euxnYLOkzwK8yV4/dU9IJEXFjp4pst3Yd1VD1EgrDQzyzobUntbei/kbPVK7XlH963X3EipOJdMuy3Jqod58IJH79Dz86dmJ9fczca37Vy1RsvOlqFqw4abSrY02NO9zt3LSxardBduuw/F4b7QZZctIZqL+fGN6FMjvAY3iIIBgZGhqzFf3MhrVJ6GVWfpWX5RjWjHGbDwee/AYOfMkbeezm/0reK5Fs/de5LlT5aKRSxWXRGzYSRElQt8U0BCRdgeNabQHP/r0PMzK0q+6GS3ZrfXjHNo54298g8ltMUGvfwcTuXliUZjfwuvF+FGX/DByXef5UlWFTWruOaqh2CYV29G1Otv5mz1Su3LrLnr2bbFnuXnnWO7SxfJ+I5AicsStegKWnn1P1HIX5K07izjRkGjkp664L3z/mkN3sPZOzXRl5V0iFpM++NJiccDiutSFxz+fPr7oVHTEyuvKrelmOyi4TafS+EZUHCNS9lX16NFK5zkrDQzuJNMyyrbrSwKwx8x13uO/I8NhzKaQkWEv9Vc+xyJ49Xm/DpbwMZu9/aHpiX/b6VCNVv8uVrZG+/n5iZGTMCaLdcnhpNc1u4HXl/SgyFJlvTESMSJpWt1Bt51ENnejbnEz9jZypnLcTr/Jw2PJW5u7njR3amOyzGLv1Kon+dKu42jkK2ZsR5b2fZae/nYiRKl1erf2DU1+JZ5/1oernDqQrv1qX5ZiMyhV4ubulWkuGkRHu+uf3jQnYDTd8kz2XH8vSV5497gKCESOMDA2NdstlA0ul/iQkqB9eja6wk8vGjN2flHfZmMrWSLN3LyxSMxt4RZ+M18gK/0FJ7yRpRUCyg/vB9pXUee0+qqHdO6cmU3+9L2te0FU7HJYaYVCvxuRyITtzL01RbbqNvJ/yyWzlYQtPOK3qHeSqXSG1WdUuuFde+WXPk/jZopeN2ScxGeorVe1ugbEtmfLz4WeeGtPaGdq2le2PPzI+yPtK3PvlTzD8zNNNH500rsYGV9jVvgf1LhuT/fvq1sNLq2lmA6/ok/EaCYpzgL8HPkhyWMk1wNntLApA0qnAZ4AScHFEfLyd8+vWw9IaNdH6G/my1gq6WofDjgztSvumx/6h5tWYXC6kdp0xMkyMDI9eeqLWCqDa+6knr9Uz7rXlnb3qa/jcAYD5m+/lyCeu293dVGWfRKtk51sZfJVHEJUN7rd/1WnN2GMeWx/4eUNHJ42poUYrp56q34MmLhszlf6Om9nAK/pkvEbOzN4IrO5ALaOUHPv3jyTncKwDfirpioi4q53z7cbD0poxkfon0xqpdTjsPV84HyKauvpltg6Gh+lL+8tHRdTcJ5H3fka7THK2hPNaPdX2Waz55j/wzMaHOPLtnxzbrVNjusv3n8fR2ZDom0kM7Uq6dTLXXJq/4qTq771J2RVIoyuXeoc2j98fsPtoscp9HSqVkiCdwE7lVrTup9LfcaPBVvTJeKrVtyjpfRFxgaT/S5UD1CPinVV+rTVFSc8Dzo+Il6bPP5DO86+rvb5vcG4MLFnRrnJ6wkSHnaeGAAAOxElEQVSPplhy6pnjDoetddmNRuvYc/mxHHTaWfRnDgEd2r6Ne7/40XH7JPKmkz0pK/sHVrlSq3WV1+QKqr9d8/0lZ92/bfQIr403/2DcdBdsuY9VLzycjdvgjme/maHoq3rkVXln+sx5C9jvuBcnoZGqvLbT5vtvZe7Swxt6P7t3gFZ/r1mNLMvsZwNUrb80OJtDX//uli2/qbLS74RWfy7bH7j+5ohYWe91eUHxyoj4tqQzq42PiLbdDlXSa4FTI+IP0+dvAk6IiD+q9noHRbGauQhgI/pnz+Wod/9jsuMuNbxrB7d/+h0T/uOo/ANr5g8u7/3lTffQ/QZZ9cLDeeTxrVx2zW2MzJw94XlWPm/m/bTqvTaqHcvP2mPSQVGkRoJC0tmU95X0D6wYPPi5RZRqbdLMlnA3etZB88eEROU9rqe7qb78ekWjQZF3CY9vk3M1s4h41QRra8TDwIGZ50vSYdn5XwRcBEmLoo21WAGm0k7JSr0eEjC1l5+Nl7cz+2/T/38bWERy+1OAM4BH21kU8FNguaRlJAGxGvjdNs/TusxU2ilZ5pDYbSouP6su7xIe1wNI+mRF0+Tbkm5qZ1ERMSTpj4CrSA6P/XxE3NnOeZpNlkPCpqtGDoyeI+mQiHgQIN3Kb/vtUCPiu8B32z0fs1ZwSNh01khQvBu4TtKDJKfCHAy8ta1VmU0hDgmb7ho54e57kpYDz04H3RMRO9pbltnU4JCwXpB/vWJA0mzgvcAfRcRtwEGSXtH2ysy6nEPCekXdoAC+AOwEnpc+fxj4aO2Xm01/DgnrJY0ExaERcQGwCyAittGOW7SZTREOCes1jQTFTkmzSE++k3Qo4H0U1pMcEtaLGjnq6cPA94ADJV0KPB84q51FmXUjh4T1qtygUHKd4HtIzs5+LkmX07si4vEO1GbWNRwS1stygyIiQtJ3I+Io4DsdqsmsqzgkrNc1so/iZ5J+o+2VmHUhh4RZY/soTgDeKGkt8DRJ91NExNHtLMysaA4Js0QjQfHStldh1mUcEma75d2PYhA4BzgMuB34XEQMdaows6I4JMzGyttHcQmwkiQkTgM+2ZGKzArkkDAbL6/r6fD0aCckfQ74SWdKMiuGQ8KsurwWxa7yA3c52XTnkDCrLa9F8RxJW9LHAmalz8tHPe3Z9urMOsAhYZYv71aopU4WYlYEh4RZfY2ccGc2LTkkzBrjoLCe5JAwa5yDwnqOQ8KsOQ4K6ykOCbPmOSisZzgkzCbGQWE9wSFhNnEOCpv2HBJmk+OgsGnNIWE2eQ4Km7YcEmat4aCwackhYdY6hQSFpNdJulPSiKSVFeM+IOl+Sb+Q5JsmWdMcEmat1cgd7trhDuC3gc9mB0o6HFgNHAHsD/xA0q9FhP/SrSEOCbPWK6RFERF3R8QvqoxaBXwtInZExBrgfuD4zlZnU5VDwqw9um0fxQHAQ5nn69JhZrkcEmbt07auJ0k/ABZVGXVeRFzegumfDZwNQP/AZCdnU5hDwqy92hYUEXHyBH7tYeDAzPMl6bBq078IuAigb3BuTGBeNg04JMzar9u6nq4AVksakLQMWI7v1W01OCTMOqOow2NfLWkd8DzgO5KuAoiIO4HLgLuA7wHv8BFPVo1DwqxzFDH1e236BufGwJIVRZdhHeKQMGuN7Q9cf3NErKz3um7rejLL5ZAw6zwHhU0ZDgmzYjgobEpwSJgVx0FhXc8hYVYsB4V1NYeEWfEcFNa1HBJm3cFBYV3JIWHWPRwU1nUcEmbdxUFhXcUhYdZ9HBTWNRwSZt3JQWFdwSFh1r0cFFY4h4RZd3NQWKEcEmbdz0FhhXFImE0NDgorhEPCbOpwUFjHOSTMphYHhXWUQ8Js6nFQWMc4JMymJgeFdYRDwmzqclBY2zkkzKY2B4W1lUPCbOpzUFjbOCTMpgcHhbWFQ8Js+nBQWMs5JMymFweFtZRDwmz6cVBYyzgkzKYnB4W1hEPCbPpyUNikOSTMpjcHhU2KQ8Js+iskKCT9jaR7JP1c0rckzcuM+4Ck+yX9QtJLi6jPGuOQMOsNRbUorgaOjIijgXuBDwBIOhxYDRwBnAr8k6RSQTVaDoeEWe8oJCgi4vsRMZQ+/TGwJH28CvhaROyIiDXA/cDxRdRotTkkzHpLN+yj+H3gyvTxAcBDmXHr0mHjSDpb0k2SborhXW0u0cocEma9p79dE5b0A2BRlVHnRcTl6WvOA4aAS5udfkRcBFwE0Dc4NyZRqjXIIWHWm9oWFBFxct54SWcBrwBOiojyiv5h4MDMy5akw6xgDgmz3lXUUU+nAu8DXhUR2zKjrgBWSxqQtAxYDvykiBptN4eEWW9rW4uijn8ABoCrJQH8OCLOiYg7JV0G3EXSJfWOiPBaqUAOCTMrJCgi4rCccR8DPtbBcqwGh4SZQXcc9WRdyCFhZmUOChvHIWFmWQ4KG8MhYWaVHBQ2yiFhZtU4KAxwSJhZbQ4Kc0iYWS4HRY9zSJhZPQ6KHuaQMLNGOCh6lEPCzBrloOhBDgkza4aDosc4JMysWdp9he+pS9JjwC9bNLn9gMdbNK1WcU2N6caaoDvrck2Nme41HRwR8+u9aFoERStJuikiVhZdR5Zrakw31gTdWZdraoxrSrjryczMcjkozMwsl4NivIuKLqAK19SYbqwJurMu19QY14T3UZiZWR1uUZiZWS4HRYakP5UUkvZLn0vS30u6X9LPJR3X4Xr+Mp3vrZK+L2n/ouuS9DeS7knn+y1J8zLjPpDW9AtJL+1gTa+TdKekEUkrK8YVUlM671PT+d4v6dxOzruijs9L2ijpjsywfSRdLem+9P+9O1jPgZKulXRXutzeVXRN6fwHJf1E0m1pXR9Jhy+TdGO6HP9N0sxO1pXWUJJ0i6T/LKSmiPBP0v12IHAVyfkY+6XDXgZcCQh4LnBjh2vaM/P4ncCFRdcFvAToTx9/AvhE+vhw4DZgAFgGPACUOlTTrwPPAq4DVmaGF1lTKZ3fIcDMtI7DO/n9ydTyIuA44I7MsAuAc9PH55aXY4fqWQwclz6eC9ybLqvCakrnKWCP9PEM4Mb07+syYHU6/ELgbQUswz8BvgL8Z/q8ozW5RbHbp4H3AdmdNquAL0bix8A8SYs7VVBEbMk8nZOprbC6IuL7ETGUPv0xsCRT09ciYkdErAHuB47vUE13R8QvqowqrKZ0PvdHxIMRsRP4WlpPx0XEDcCvKgavAi5JH18CnN7BetZHxM/Sx1uBu4EDiqwprSUi4qn06Yz0J4AXA18vqi5JS4CXAxenz9XpmhwUgKRVwMMRcVvFqAOAhzLP16XDOkbSxyQ9BLwB+FC31JX6fZKWDXRPTVlF1tSNn0fWwohYnz7eACwsoghJS4FjSbbeC68p7eK5FdgIXE3SKtyU2TgqYjn+HclG7Ej6fN9O19Tfzol3E0k/ABZVGXUe8GckXSodl1dXRFweEecB50n6APBHwIeLril9zXnAEHBpu+tptCabmIgISR0//FHSHsA3gD+OiC3JhnKxNUXEMHBMuu/tW8CzO11DlqRXABsj4mZJJxZVR88ERUScXG24pKNI+q9vS7+oS4CfSToeeJhk30XZknRY2+uq4lLguyRB0da66tUk6SzgFcBJkXaSFl1TDW1ffl0670Y8KmlxRKxPuy03dnLmkmaQhMSlEfHNbqgpKyI2SboWeB5J125/ugXf6eX4fOBVkl4GDAJ7Ap/pdE093/UUEbdHxIKIWBoRS0maccdFxAbgCuDN6VFGzwU2Z5rGbSdpeebpKuCe9HFhdUk6laQZ/KqI2JYZdQWwWtKApGXAcuAnnagpR5E1/RRYnh6dMhNYndbTLa4Azkwfnwl0rFWW9rF/Drg7Ij7VDTWldc0vH8UnaRZwCsn+k2uB1xZRV0R8ICKWpOum1cB/RcQbOl5Tp/fed/sPsJbdRz0J+EeSfsrbyRxR06FavgHcAfwc+DZwQNF1kewQfgi4Nf25MDPuvLSmXwCndbCmV5ME/A7gUeCqomtK5/0ykiN6HiDpIuvYvCvq+CqwHtiVfk5/QNLPfQ1wH/ADYJ8O1vMCkp3EP898j15WZE1pXUcDt6R13QF8KB1+CMkGxv3AvwMDBS3HE9l91FNHa/KZ2WZmlqvnu57MzCyfg8LMzHI5KMzMLJeDwszMcjkozMwsl4PCeoak05VcHbju2baSzlJ6td4JzuvE8pU+M8NmS3pC0p4Vw/9D0u80My2zTnJQWC85A/hh+n89ZwETDopqIjlB8SqS8z4AkLQXyXkF327lvMxayUFhPSG9rtALSE42W10x7v2Sbk/vQ/BxSa8FVgKXKrkXyCxJa7X7PiUrJV2XPj5e0v+m9wr4kaRn1SnlqxXzfzXJCYLbGpmWpPMlvSfz/I70wnpIemN6P4VbJX1WUqm5T8msOgeF9YpVwPci4l7gCUkrACSdlo47ISKeA1wQEV8HbgLeEBHHRMQzOdO9B3hhRBxLcnXfv6pTx1XAcZL2TZ+vJgmPiUxrlKRfB34HeH5EHAMMk1xx2GzSeuaigNbzziC5mBok94Y4A7gZOBn4QtotRERU3rehnr2AS9LrcgXJPQxqioidkq4AXivpGySX2L5qItOqcBKwAvhpenHLWRR4UT2bXhwUNu1J2ofkRi9HpZeuLgEh6b1NTGaI3S3wwczwvwSujYhXp11A1zUwra8Cf05yza7LI2JXE9PK1pGtRcAlEfGBBuZv1hR3PVkveC3wpYg4OJKrBB8IrAFeSHJzmt+TNBtGQwVgK8ltOsvWkmyxA7wmM3wvdl/i+awG67mO5Eq272B3t1Oj01pLcltTlNwrfVk6/BqSVsqCdNw+kg5usB6zXA4K6wVnkNyEJusbwBkR8T2Sy1vflN7ZrLyj+F+BC8s7s4GPAJ+RdBNJ/3/ZBcBfS7qFBlvoETFCchvLfYHrm5zWN4B9JN1JciOre9Np3gV8EPi+pJ+TBGDHbttr05uvHmtmZrncojAzs1wOCjMzy+WgMDOzXA4KMzPL5aAwM7NcDgozM8vloDAzs1wOCjMzy/X/AWtQFfjIsYQYAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "actual, predicted, residual, rows = zip(*validation.data)\n",
    "data = {'actual': actual, 'predicted': predicted}\n",
    "data_frame = pd.DataFrame(data)\n",
    "\n",
    "plot = data_frame.plot.scatter(\n",
    "    x='actual',\n",
    "    y='predicted',\n",
    "    legend=False,\n",
    "    color=dr_light_blue,\n",
    ")\n",
    "plot.set_facecolor(dr_dark_blue)\n",
    "\n",
    "# define our axes with a minuscule bit of padding\n",
    "min_x = min(data['actual']) - 5\n",
    "max_x = max(data['actual']) + 5\n",
    "min_y = min(data['predicted']) - 5\n",
    "max_y = max(data['predicted']) + 5\n",
    "\n",
    "biggest_value = max(abs(i) for i in [min_x, max_x, min_y, max_y])\n",
    "\n",
    "# plot a diagonal 1:1 line to show the \"perfect fit\" case\n",
    "diagonal = np.linspace(-biggest_value, biggest_value, 100)\n",
    "plt.plot(diagonal, diagonal, color='gray')\n",
    "\n",
    "plt.xlabel('Actual Value')\n",
    "plt.ylabel('Predicted Value')\n",
    "plt.axis('equal')\n",
    "plt.xlim(min_x, max_x)\n",
    "plt.ylim(min_y, max_y)\n",
    "\n",
    "plt.title('Predicted Values vs. Actual Values', y=1.04)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also plot residual (predicted minus actual) values against actual values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0.5,1.04,'Residual Values vs. Actual Values')"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYoAAAEfCAYAAABf1YHgAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3X18nHWZ7/HPd2aStCmFltJCS4EUKLqlKE8+oYICHgUf0npYl66roK5sFT3K6qroOfvo7tGVXXTPgshZkeqyKkeBclREQEBdFlwQlAIeoLRIoaVSWwikTTOT6/xx35NOkslkmqd7Jvm+X6+8OnPPPXeuTNK55vd0/RQRmJmZDSeXdQBmZtbYnCjMzKwmJwozM6vJicLMzGpyojAzs5qcKMzMrCYnCquLpAckvW6Yx14nadM4fZ/bJP3xeFxrMq89HUk6V9LPmu3atvecKKYYSRsl7ZT0vKQtkq6UtM9YrxsRR0fEbeMQ4qhIOjv92TToeEHSVklvySq2LCjxmKQH9+I545bQR/g+MyTtkHRqlcculvSdiY7BxpcTxdT01ojYBzgWOA64MON4xsN1wBzglEHH3wQE8MNJjyhbJwMLgMMlvSzrYCpFxC7g28C7K49LygOrgDVZxGWj50QxhUXEFuBGkoQBgKQ2SRdJ+o2kpyVdJmlm+tgBkr6Xfhr8naSfSsqlj22UdHp6e2baUtmefqId8EYlKSQdWXH/SkmfTW/PTb/Hb9Pnf0/S4jp+ll3A1Qx680nv/1tEFPfm2pL+UtK/VtzvSOMupPf3k/RVSZslPSnps+kbHZKOlHS7pGclPSPp28N8jxskfWjQsV9KenvaIrg4bQ09J+l+SctHeh0qnAOsBX6Q3q78HvtL+pqkp9LX4TpJs4AbgEVpa/N5SYsqfzfpcwe0OiR9StJ6SV2SHpS0ss741gD/VVJ7xbE3krzn3LA31x78u0mPDehGlPReSQ+lP++Nkg5Lj4/1dTacKKa09E3yDODRisOfA44iSR5HAgcDf54+9jFgEzAfOBD4NMmn9cH+Ajgi/Xojg96oRpADvgYcBhwK7AT+uc7nrgHOqkhs+wFvZc8n1LFce7ArgSLJa3Qc8F+A8hvT3wA/AuYCi4H/Ncw1vknyCZo03mVpbN9Pr3cyye9iP+AdwLZ6AkvffM8Crkq/zpbUWnHKN4B24GiSVsfFEfECyd/CUxGxT/r1VB3fbj3w2jTGvwL+VdLCkZ4UEXcAm4G3Vxx+F2lSH8u1B5PUSfK3+naSv92fkrz2MIbX2fZwopiarpPUBTwBbCV5Y0eSgPOACyLidxHRBfwdcHb6vF5gIXBYRPRGxE+jejGwdwB/m17jCeCf6g0sIrZFxHcjojv9/n/L0O6k4Z7778DTQPmT5zuAhyPivrFeu5KkA4EzgY9GxAsRsRW4mIGv02HAoojYFRHDDbpeCxxb/nQLvBO4JiJ60mvMBl4MKCIeiojNdYb4dqCHJFl9H2gB3pzGvpAkIayOiO3p7/H2un/4QSLi/0TEUxHRFxHfBh4BXl7n079O2gKUtC/QSUW30xivXWk18D/T17BI8jddft3H8jpbyolialoREbOB15H8BzkgPT6f5JPmPWn30g6Svv356eNfIGl9/EjJQOmnhrn+IpIkVPZ4vYFJapf0FUmPS3oO+Akwp9ytU4f+Nx+ST6hfH8drlx1G8ua7ueJ1+grJp3OATwACfq5kNth7q10kTVbfZ0+CWUXSAiAifkzS2rkE2Crp8vTNtB7nAFdHRDHtkvsue1p1hwC/i4jt9f+4w5P0bkn3VbwOy9nz9zSSbwCvl7SIpAW0PiLuHadrVzoM+FLFdX5H8vs5eIyvs6WcKKaw9JPklcBF6aFnSLpjjo6IOenXfunANxHRFREfi4jDgbcBfyrptCqX3kzyhlR26KDHu0kSUtlBFbc/BrwIeEVE7EvSLQDJf+x6fAM4TdKrgFeSvvGO4tov1IjxCZJP7AdUvE77RsTRkIz9RMT7I2IR8CfApaoYkxnkm8CqNN4ZwK3lByLinyLiBGAZSdfIn430w6fdiacCf6RkVtsWkjfhMyUdkMa+v6Q5VZ5erXU47OuQfiL/38CHgHkRMQdYR52/q4h4nKQb6I9Iknp/a2Ivr/1C+m+t39efVPyu5kTEzLT7a1Svsw3kRDH1fRF4g6SXRkQfyX/OiyUtAJB0sKQ3prffomSgVsCzQAnoq3LNq4ELlQweLwY+POjx+4A/lJSX9CYGdv/MJklWOyTtT9otVq+I2Aj8jOQN+KZ0wH40174POFnSoelYR//MsLRr4kfAP0jaV1JO0hGSTgGQ9PvaM0i+neQNuNrrBMlg82HAXwPfTn8HSHqZpFdIaiF5I9xV4xqV3gU8TJIQj02/jiIZW1qVxn4DSfKaK6lFUjlhPg3MS3/eytfhTCUD4AcBH614bFb6s/02jfk9JJ/698YakmTwagYm9bqvHRG/BZ4kSY75tAV3RMUpl5H8PR6dXms/Sb+f3h7t62wVnCimuPQ/2dfZM2D9SZLupTvT7pmbSd50AJam958H/gO4NCJuZai/Iulu2kDyhvqNQY9/hGSQeQdJv/x1FY99EZhJ0rq5k9FNa11D8ub79UHH6752RNxEMoXzV8A9wPcGnfJuoBV4kCQZfIdk/AaSWV53SXoeuB74SEQ8Nsz36QGuAU4H/q3ioX1JkvZ2ktdyG0nXH5I+LemGYUI/h+T3sqXyi+TNstz99C6Svvlfk4xRfTSN5dckCfaxtJtmEcnv7pfARpLfZf8Mroh4EPgHkr+Fp4FjgH8fJq7hfBfYH7ilcmxgFNd+P0lLYBvJIP0dFde6Fvg88K30b3odyTgN1HidrX7yxkVmZlaLWxRmZlaTE4WZmdXkRGFmZjU5UZiZWU1OFGZmVpMThZmZ1eREYWZmNTlRmJlZTU4UZmZWkxOFmZnV5ERhZmY1OVGYmVlNThRmZlZTYeRTGp/yLaGWGVmHYWbWVKLn+WciYv5I502NRNEyg7bFJ2QdhplZU9m1/va6tjF215OZmdXkRGFmZjU5UZiZWU1OFGZmVpMThZmZ1eREYWZmNTlRmJlZTU4UU1ihfTbti46g0D4761DMrIlNiQV3NtTc5SfR0bmaKBVRvsDGtZexfd0dWYdlZk3ILYopqNA+m47O1eRb2ijMmEW+pY2OztVuWZjZqDhRTEGtcxYQpeKAY1Eq0TpnQUYRmVkzyzxRSMpLulfS99L7SyTdJelRSd+W1Jp1jM1m946tKD+wV1H5PLt3bM0oIjNrZpknCuAjwEMV9z8PXBwRRwLbgfdlElUTK3Z3sXHtZZR6eyju6qbU28PGtZdR7O7KOjQza0KZDmZLWgy8Gfhb4E8lCTgV+MP0lDXAXwJfziTAJrZ93R10PXY/rXMWsHvHVicJMxu1rGc9fRH4BFAeZZ0H7IiIcgf7JuDgLAKbCordXU4QZjZmmXU9SXoLsDUi7hnl88+TdLeku6PUO87RmZlZWZYtilcDb5N0JjAD2Bf4EjBHUiFtVSwGnqz25Ii4HLgcIDdjdkxOyGZm009mLYqIuDAiFkdEB3A28OOIeCdwK3BWeto5wNqMQjQzMxpj1tNgnyQZ2H6UZMziqxnHY2Y2rWU9mA1ARNwG3Jbefgx4eZbxmJnZHo3YorAG4IKCZlbWEC0Kayxzl5/Eks7VRF8fyuXY4IKCZtOaWxQ2QKF9NktWfJBcSxv5tpnkWtpYsuKDblmYTWNOFDbAzIM6qtSJKjDzoI5sAjKzzDlR2CDay+NmNtU5UdgAO7dsqFKivMjOLRsyisjMsuZEYQMUu7vYcN2lSeXZnl2UenvYcN2lrhllNo151pMN4cqzZlbJicKqcuVZMytz15OZmdXkRGFmZjU5UZiZWU1OFGZmVpMThZmZ1eREYZPKVWnNmo+nx9qkmbv8JDo6VxOlIsoX2OiqtGZNwS0KmxSF9tl0dK4m39JGYcYs8i1tdHSudsvCrAk4UdikaJ2zoEoNqRKtcxZkFJGZ1ctdTzZhCu2z+8uA7N6xtUr58jy7d2zNKDozq5cThU2IauMRG9delh4roXyejWsvc5kQsyagiMg6hjHLzZgdbYtPyDoMSxXaZ3PMBZeQb2nrP1bq7eH+i88HcLFBswaxa/3t90TEiSOd5xaFjbv+8YiKRFEej+h+ar0ThFmT8WC2jTuPR5hNLU4UNu6K3V1sXHtZsvnRrm5KvT0ejzBrYu56sgnhzY/Mpg4nCpsw3vzIbGpw15M1HNeDMmssblFYQ5m7/CSWdK4m+vpQLscG14Myy5xbFNYwCu2zWbLig+Ra2si3zSTX0saSFR90y8IsY04U1jBmHtRRZVptgZkHdWQTkJkB7nqyjFXWgwINc9Zwx81sMjhRWGYG14P6zQ1rktuFlv5zolRk55YNGUZpZu56skxU25/i0DPO4Tc/uIK+3h5Ku3vo6+1hw3WXeoqtWcacKCwTw+1PUZi1HwFE9NH85SrNpgYnCsvEcPWgFp28MmlltM30LnhmDcKJwjJRrR7U5p9cS593wTNrOB7MtswMrgcFsPDklQPOcdVZs+y5RWGZKnZ39e9RUU/VWZf3MJt8blFYQ6lVddblPcyykVmLQtIhkm6V9KCkByR9JD2+v6SbJD2S/js3qxgtG5WtjLK2eQtZsuJ8l/cwy0CWXU9F4GMRsQx4JXC+pGXAp4BbImIpcEt636axuctP4ugPfAHl8wOOu7yH2eTILFFExOaI+EV6uwt4CDgY6ATWpKetAVZkE6E1gv5CgYUWpGqlPFzew2yiNcRgtqQO4DjgLuDAiNicPrQFODCjsKwBzDxoyZD1FmXRV3J5D7NJkHmikLQP8F3goxHxXOVjERFQfYGupPMk3S3p7ij1TkKklo0a67Nj+Mc8O8ps/GQ660lSC0mSuCoirkkPPy1pYURslrQQqDqJPiIuBy4HyM2Y7WoPU9TOLRuHFAos6yv20jpnwZBaUIOLDW4cNDuqsmKt60iZjSzLWU8Cvgo8FBH/WPHQ9cA56e1zgLWTHZs1jmJ3Fxuuu5S+3t3EoBZEeTFeZeuhbd5COlZ8YECxwcoyIHOXn8QxF1zCUe/+DMdccAlzl5+UxY9l1lSybFG8GngXcL+k+9JjnwY+B1wt6X3A48A7MorPGkR5bcUBJ5zOwpNXEqUSyufZuPYyZh9+TH/rIVdoBQnlBs6OqiwDUq5YS0tb//2ux+53y8KshswSRUT8jOGnrJw2mbFY4yt2d7Hlp9fyzD03Dyj5ccwFlwx446+m3PLor1hbcW45iThRmA3PK7OtqZRLfQC0LzpiyBt/pXJXVdfGB/ufU61irWtJmdWW+awns9GqVqq8kiQksd+Rx9I2b2FdtaTMbCi3KKxpld/4kzGKErlCS/8YxeDFebMWL6Vn2+aataTAM6LMqnGisKY2+I1/5sIlHPVHnx5y3q5nnuq/Xdl9VclFB82qG7HrSdKBkr4q6Yb0/rJ0RpJZQxhQRHCYRXj5tpk1r9E2byFLVrrooFk19YxRXAncCCxK7z8MfHSiAjIbi3zbrL06DuWig38/ZFqt8gXmHn2Sk4VNe/UkigMi4mqgDyAiikBpQqMyG6VSzwvDPNJX9WihfTYdnavJFVqrFh08+PRVXphn0149ieIFSfNIi+5IeiXw7IRGZTZK5ZIfgx3+9g8x7/jTaF90BO0HH8n+Lz2ZtnkL96ytqEIShbaZQ1Z3m0039Qxm/ylJWY0jJP07MB84a0KjMhulcsmPJZ0fQGlpckmopY2Ot76/v/5T2W/vuWXIFNvy+osBLYzAC/Ns2hqxRZHuGXEKcBLwJ8DREfGriQ7MbLS2r7uDx6755yHHJfXva1H+mn/CaTx5y7f611b09fbw9H98f8hzcy2tlHq6JyN8s4YzYotC0rsHHTpeEhHx9QmKyWzMSj076z63uPN57r/4/P4ptq1zFjD/xDeQb92z4ruvdzf5tvaJCNWs4dXT9fSyitszSOow/QJworCGtXPLhmHLkw/2wqZHhq6tGDyuLVzqw6aterqePlzx9X7geGCfiQ/NbPTKYxWl3h6KPbvoK/b2f0VE/9fTd91Az7bNQ57rUh9me4xmZfYLwJLxDsRsvA1etQ3JgDQSMw5YxAubHhmSJCqf2715A7MWL615ntl0UM8Yxf9lz36UOWAZcPVEBmU2XgZ3KZVvdz/5aNXzy7WeZh7UwaFnnDPsLnlm00k9LYqLKm4XgccjYtMExWOWiUL77P6NkSiVyLXNTKbHeoMjs5ETRUTcPhmBmGWlXAxQ5dXZVca/vcGRTWfDJgpJXezpchrwEBARse+ERWU2SfpLeNTYIQ8gV2jp35+7dc4CSj3d5Nva+/91WXKbyoZNFBHhegU25VXbHrU6sf9LXsvBp50NkSzAi1IvyrfQ17sbhMcxbMqqe9aTpAUk6ygAiIjfTEhEZpOo2i551Up4KJ/nkDe+e+CxQitA/8I8j2PYVFXPfhRvk/QIsAG4HdgI3DDBcZlNivKaib7engEJYnAl2WrHBiuPY5hNNfVUj/0b4JXAwxGxhGRl9p0TGpXZJNq+7g4e+eZFlHbvGnC8vCivXsrnvXrbpqR6EkVvRGwDcpJyEXErcOIEx2U2qXZu2YByA/87jNSKKCeS0u4er962Ka2eMYodkvYBfgJcJWkryepssymj3AXV0bka+oJca9uAJFG19Diw6eZv0rVhXX9Lon3REZ4BZVNOPYmiE9gFXAC8E9gP+OuJDMosC+WSHzMP6uDIVX9GvmIm1OB9LMq6Nj5A91Prmbv8JDo6V3slt01Jw3Y9SbpE0qsj4oWIKEVEMSLWRMQ/pV1RZlNOsbuLrsfuH1IU8Dc/uILoG7oD8IvO/XMOOP5UOjpXk29pozBjlnfEsymnVoviYeAiSQtJajt9MyLunZywzLI1uKBgst6iBBWtCknkW9o49M3vo6+3Z8BaDK/ktqlk2BZFRHwpIl5FsrvdNuAKSb+W9BeSjpq0CM0yUuzuovup9RS7uyj1dJNraa16nnL5IfteeAaUTSX17EfxeER8PiKOA1YBK4CHJjwyswaSb2tPWg3DeHb9ugH7XGz/9d1Vzyu0z6Z90RHulrKmUk+Z8QJwBnA2yRqK24C/nNCozBrM7h1bocZU2TlHvmTAjKh5y09i/xefyIaKQW0PeFuzqjWY/QZJVwCbgPcD3weOiIizI2LtZAVo1ggqd72rtghPufzA+xK5ikHtcvFBD3hbM6rVorgQ+DfgYxGxfZLiMWtY29fdQWHmPhx65nsHHK+5KK+irMfg4oOVj5UHzT34bY2oVvXYUyczELNm8Nxj9+/V+eXy5MCQdRjK55l5UAcves9fuDvKGlo9JTzMLNWzbTNP33XDXteBqiw+WOrZRV9vD0/ccCWHnnGOu6Os4TlRmO2lTT9cw7p/voCt//kj+np3U+rZNWzS6Cv20jpnAYX22bTNPRCAIAigdc586OsbcL4r0Fojqns/CjPbo2fbZp74wRVsvu3/sOjUs5l/wmlVz6vsXsqlW62WP50tfO3Kqud7/YU1mlqznrokPVflq0vSc5MZpFmjKnZ38cy9tw45Xu6a2vbLn3LYme8h39JWc4+LiKDPFWitQXkrVLMx6n7yUZ599D72O/JYYGACOODYU6oWE6xK7gm2xlT3X6akBZIOLX9NZFBmzebRqz7HhrWX0VfcPeB4DBqDGI4kcoUWlqw834PZ1nAaditUSW+S9P8kPSrpUxP9/czG6rmH7xlybPBCvErVZk4pl2efw5aNe2xmY9GQW6FKygOXkJQOWQaskuT/PdbQKldvl8uTP3X7d/b6Ovsc9ntuVVhDqafztDcitknq3wpV0hcnOK6XA49GxGMAkr5FsoHSgxP8fc3GpFp58kWvO6vqOMVwK7rnH/d65p9wqhffWcNo1K1QDwaeqLi/CXjFBH9Ps3FR7O7qn7mUn7lPze6nwSSRb5sBQEfnaroeu9+zoCxz9XQ9dQI7SbZC/SGwHnjrRAZVD0nnSbpb0t1R6s06HLOqkvLku0c8r9p4hRffWaMYsUUREZWthzUTGEulJ4FDKu4vTo/1i4jLgcsBcjNm119LwWwSJeXJ6zs3Sr2osGdzJBUKlHq6Jygys/rVM+upcuHdLkmlSVhw95/AUklLJLWS7IVx/QR/T7NxN1J58krdW34zYPMjKcey1Z9n7vKTJilas+rqaVH0T79QMvrWSTILasJERFHSh4AbgTxwRUQ8MJHf02yilAe4Dzj+dBad8nYiglxL65DB7FkHHzHgmPIFyBdYsuIDdG/eQM+2zUCyS57Lkttk0t5UwOx/knRvujVqQ8jNmB1ti0/IOgyzERXaZ7Pv0uM47M3vI986o/94+f9htZlQEUFfqZeN130ZgCWdq9MWhwbsoGe2t3atv/2eiDhxpPPq2Qr17RV3c8CJwK4xxGY2bRW7u3jukXuHbKtaa/MjSeQLrXR0rka5Arn8nllUS1ae75lRNuHqmfX01oqvNwJdJN1PZjYK/XtTFHcPnelUY58LIZQb+F/WK7ltMtQzRvGeyQjEbDrZvu4OWvebz+LTVw04XrNlUWipenzmgYey46G7xjU+s0rDJgpJ/wsYdgAjIv7bhERkNk3s+PXPhySK0ahnnYbZWNTqerobuAeYARwPPJJ+HQu01niemdVh8Laqo5lYAknCKSu0z6Z90REDakVVO2a2N0ac9STpTuA1EVFM77cAP42ICZ0iuzc868maWdu8hRxw/KkcdNJba3Y9RQQ7Hr6XOUftmXC49Z5b2PaLH7N7x1ZmH34MHZ2riVIR5QtsXHsZwJBjniVlZeM26wmYC+wL/C69v096zMzGQc+2zTx501Xse/gxtB/UAQw/VtHzu81E8ehkemwuz/xjT2He8leRyxdAOXKFFmhpA5IEISDX0jbgmGdJ2d6qZ9bT54B7JV0paQ3wC+DvJjYss+nnoa98ik23fLPmOQte9gZyLW3kW2eQK7SQK7RQmDGLXEvbkAq10RdDNk5y/SgbjRETRUR8jaRy67XANcCrImKyaj6ZTSvbfvFjolSs+lhEH33F+gtgKldlOm0+n9SfMtsLwyYKSS9O/z0eWERS9vsJYFF6zMzGWbG7iw3XXUpftdpQfZF0Iw0jSsUBmyZtXHsZGwZtpLRx7WXudrK9VmuM4k+B84B/qPJYAKdOSERm09z2dXdQ7O7iyD/4GPm2mf3Ho9RLrqLsB6TlPXp7kHI8dfs1bH/oTvJt7QPqQFVupOQkYaMxbKKIiPPSf18/eeGYGcDOLRthcLfRMAvulCugfJ6FJ69g4Skr2bj2MrqfWt//eOVGSmajUU+Z8d+XNDu9/d8lXSOpYQoCmk1F1fbf3nrXD4ecJ4lcoZDUg2qdQb6ljY4VH6Bt3sL+c7yOwsaqnnUUv4qIl0h6DfBZ4AvAn0dEw2xN6nUUNlVVlhTPz9yH5R+6uOZaC0jrRZWKbLjuUiBdR9HXh3I5r6OwAcZzHUUp/ffNwOUR8X1Jnx1TdGZWl8puo9Y5C+jr7RlQnrwaSajQwpKV5wMkayxSS1Z8kGJ3Fzu3bHR3lNWtnkTxpKSvAG8APi+pjfrWX5jZOEq2VR3Ymqi1j4Vy+aHH8gWOPPvjILl1YXWr5w3/HSQ7zb0xInYA+wN/NqFRmdkQ1cYtnrjx60Sp+toKSUMSyICxjM7VHrewutRTZrxb0lbgNSRFAYvpv2Y2ycrbqlZOd+19fgcdnavJFYZur1pLeZW2u6BsJPXMevoL4JPAhemhFuBfJzIoMxtesbuL7qfW97/Bb193B/dffD6/W3fHXlWiVT5PqafbM6JsRPWMUawEjiOp8UREPFWeLmtmjWPuslfUNSOqb3cP5MRv77mFZas/78qyNqJ6xih2R/LxJAAkzZrYkMxsb+1z2LKqg9fVPHHzVTx42SeZf8Jp5FvaKMyY5TELq6meRHF1OutpjqT3AzcD/zKxYZnZ3mjZZ07d53Y9dj/5tvYhxQddWdaGU89g9kWS3gA8B7yIZLHdTRMemZnV7bnHflX3ua37HcDuZ58ZUhLElWVtOPWMUZAmhpsAJOUkvTMirprQyMysbuVtVQ98xRl7Dvb1QS43ZNxidsfRHPiqM6Ev0qKCu0H0V5atXA3uGVEGNUp4SNoXOB84GLieJFGcD3wc+GVEdE5WkCNxCQ+zRNu8hcxavJQXNj1CfsYsfu+PPzsgUUQEfcXd5CvKlfcVd/PAlz9Bz7bNzF1+krdOnUbGo4THN4DtwH8Afwx8GhCwIiLuG5cozWxc9WzbTM+2zQC0LzpiSMmPKPUOmTrbV+qjdb/5lHY+T0fn6iSJeOtUq1ArURweEccASPoXYDNwaETsmpTIzGxMqpX8UL6F/KDJUfnWNpau+jhP/eTaZIC7orXhRXkGtWc99dcFiIgSsMlJwqx5VJb8KO3qJiIGlPWorBOVa2lj4ckrBxQQhNoD3C5fPn3UalG8VNJz6W0BM9P7AiIi9p3w6MxsTMolP/ZdehyHnnEuhRl7lkENHuSOUonNP7uehaesJEollM8Pu3WqxzKml1o73NW3esfMGlqxu4vnHrkXveWPa56nfJ5nfnEz2x+6s39AvDzeUanQPttjGdNMXdNjzay5lbuhlqz4AMq3DJkJBfDbe25h9uHHjNhSaJ2zwGMZ04z3lTCbJravu4MHvvwJoq804Hh53GL+CaexJG0p1CrrsXvHVrQXYxnW/JwozKaRnm2b2XDtJfQVh06Tjb4g+voGHqtS1qPavhjDjWXY1OCuJ7NpZvu6O+jevIGjP3jRgJZBLp+HGJgocvk8+RmzKLTPHpAIqu2LYVOXE4XZNFTa+TxUqcrw+A1rOPSMc4hSiVyhBZTjiHd8FOULbP7JtTxzz839SaFyP2+b2tz1ZDYNtc5ZQF9x94BjfcVedm7ZyP0Xn8/6q/+RiD5yhZb+8YqDT/0DXnLBJcxdflJGUVtWnCjMpqFaA9LF7q5kgd6gMuTlhXnet2L6caIwm4ZGGpCulkj6Bd63YprxGIXZNFVrQLqcSDo6V5MrtA5Yd5FraaXU0+1y5NNIJolC0heAtwK7gfXAeyJiR/rYhcD7gBLw3yLixixiNJsOag1Ib18tUKgMAAALv0lEQVR3B8XuLpa+81MDWhdRKjJ32StZePJKl/CYJrLqeroJWB4RLwEeBi4EkLQMOBs4GngTcKkklxIxy8juZ58Zshe38gUWnbzS+21PI5kkioj4UUSUR8ruBBantzuBb0VET0RsAB4FXp5FjGYGrfvNr3q8noV5Za4y2/waYYzivcC309sHkySOsk3pMTPLxDA7YOYHtzKql/BwldmpYcJaFJJulrSuyldnxTmfAYrAXu+/Lek8SXdLujtKvSM/wcz22s4tG4dMk41Skcd/cOWIJTwqq8y6i6q5TViLIiJOr/W4pHOBtwCnxZ6iM08Ch1Sctjg9Vu36lwOXQ7Jn9ljjNbOhit1dbLjuUpZ0rib6AuXEhrRV8Oyvf07rnAWUerrJt7UPKfPhKrNTR1aznt4EfAI4JSK6Kx66Hvg3Sf8ILAKWAj/PIEQzSw03jbbY3TWkLPkTN1xJ95bH2b1jq6vMTiEaXEFyUr6p9CjQBmxLD90ZEavTxz5DMm5RBD4aETeMdL3cjNnRtviEiQrXzKootM/mmAsuSTYwSkUEpZ7u/vEIIE0ke3bM8xhF49i1/vZ7IuLEkc7LJFGMNycKs8nXvugIjnr3ZwZsr1qp1NvD/RefD+CFeQ2q3kTRCLOezKwJ1SzzwZ7xiO6n1jtBNDnXejKzUamsF1Xa1T1kI6RcocXjEVOEWxRmNmrlge6ZB3Ww9A8/iQotWYdkE8AtCjMbk3JZ8mr7W7jK7NTgRGFmY+apsFObE4WZjdlI+1tYc/MYhZmNi1r7W1hzc6Iws3FTa38La17uejIzs5qcKMzMrCYnCjPLnDc3amweozCzTHlzo8bnFoWZZcabGzUHJwozy0z/5kYVau2/bdlwojCzzHhFd3NwojCzzHhFd3PwYLaZZcoruhufE4WZZc4ruhubu57MzKwmJwozM6vJicLMzGpyojAzs5qcKMzMrCYnCjMzq8mJwszManKiMDOzmpwozGxK8J4WE8crs82s6Q23p0WhfbZLg4wDJwoza2qVe1rQ0gaQ3G+dwSFnnOsNkcaBu57MrKnNPGgJ0dc34FiUShx65nu8IdI4caIws6Y1d/lJLF31cfKtMwYcV6FAFL0h0nhxojCzplTucsq1tCEJgIig1NvDb35wJeTzA873hkij5zEKM2tK/duopuMSAH27e3j02xfR9dj99O3emQ5wl1A+7w2RxsCJwsyaUrVtVMmJnVs2At4QaTy568nMmlI926gWu7vofmq9k8QYuUVhZk3LrYbJ4URhZk3N26hOPHc9mdmU5bIe48MtCjObkoYr62F7L9MWhaSPSQpJB6T3JemfJD0q6VeSjs8yPjNrTpVlPbwye+wySxSSDgH+C/CbisNnAEvTr/OAL2cQmpk1uf41FhW8Mnv0smxRXAx8AoiKY53A1yNxJzBH0sJMojOzplVtjYVXZo9eJolCUifwZET8ctBDBwNPVNzflB4zM6tbPWssrH4TNpgt6WbgoCoPfQb4NEm301iufx5J9xQU2mqfbGbTjtdYjJ8JSxQRcXq145KOAZYAv0wLeS0GfiHp5cCTwCEVpy9Oj1W7/uXA5QC5GbOj2jlmNr15jcX4mPSup4i4PyIWRERHRHSQdC8dHxFbgOuBd6ezn14JPBsRmyc7RjMz26PR1lH8ADgTeBToBt6TbThmZpZ5okhbFeXbAZyfXTRmZjaYS3iYmVlNThRmZlaTE4WZmdXkRGFmZjUpGT9ubpJ+Czw+QZc/AHhmgq49GZo9fmj+n8HxZ8vxD++wiJg/0klTIlFMJEl3R8SJWccxWs0ePzT/z+D4s+X4x85dT2ZmVpMThZmZ1eREMbLLsw5gjJo9fmj+n8HxZ8vxj5HHKMzMrCa3KMzMrCYnihokfVjSryU9IOnvK45fmO7r/f8kvTHLGEfSrPuSS/pC+tr/StK1kuZUPNYUr7+kN6UxPirpU1nHMxJJh0i6VdKD6d/8R9Lj+0u6SdIj6b9zs461Fkl5SfdK+l56f4mku9Lfw7cltWYd43AkzZH0nfRv/yFJr2qE19+JYhiSXk+yNetLI+Jo4KL0+DLgbOBo4E3ApZLymQVaQ5PvS34TsDwiXgI8DFwIzfP6pzFdQvJ6LwNWpbE3siLwsYhYBrwSOD+N+VPALRGxFLglvd/IPgI8VHH/88DFEXEksB14XyZR1edLwA8j4sXAS0l+jsxffyeK4X0A+FxE9ABERHmz3U7gWxHRExEbSEqivzyjGEfStPuSR8SPIqKY3r2TZBMraJ7X/+XAoxHxWETsBr5FEnvDiojNEfGL9HYXyZvUwSRxr0lPWwOsyCbCkUlaDLwZ+Jf0voBTge+kpzRs/JL2A04GvgoQEbsjYgcN8Po7UQzvKOC1aZP1dkkvS483xb7eU2xf8vcCN6S3myX+ZomzKkkdwHHAXcCBFRuIbQEOzCisenyR5MNRX3p/HrCj4kNHI/8elgC/Bb6Wdp39i6RZNMDrn/l+FFkaYV/vArA/SRP8ZcDVkg6fxPBGNNH7kk+0WvFHxNr0nM+QdIlcNZmxTWeS9gG+C3w0Ip5LtywGkj1jJDXkVElJbwG2RsQ9kl6XdTyjUACOBz4cEXdJ+hKDupmyev2ndaIYbl9vAEkfAK5JN1P6uaQ+kporde/rPdEmel/yiVbr9QeQdC7wFuC02DOPu2HiH0GzxDmApBaSJHFVRFyTHn5a0sKI2Jx2U24d/gqZejXwNklnAjOAfUn6/OdIKqStikb+PWwCNkXEXen975Akisxff3c9De864PUAko4CWkkKc10PnC2pTdISkkHhn2cWZRVTYV9ySW8i6UJ4W0R0VzzU8K9/6j+BpemMm1aSAfjrM46pprQ//6vAQxHxjxUPXQ+ck94+B1g72bHVIyIujIjF6d/82cCPI+KdwK3AWelpjRz/FuAJSS9KD50GPEgDvP7TukUxgiuAKyStA3YD56Sfah+QdDXJL7AInB8RpQzj3FvNsi/5PwNtwE1pq+jOiFgdEU3x+kdEUdKHgBuBPHBFRDyQcVgjeTXwLuB+Sfelxz4NfI6k6/V9JFWa35FRfKP1SeBbkj4L3Es6WNygPgxclX64eIzk/2eOjF9/r8w2M7Oa3PVkZmY1OVGYmVlNThRmZlaTE4WZmdXkRGFmZjU5Udi0IWlFWkn3xXWce66kRWP4Xq8rVy+tONYuaZukfQcdv07SH+zNtcwmkxOFTSergJ+l/47kXGDUiaKadOHgjcDK8rG0ENxrgP87nt/LbDw5Udi0kNYveg1JiemzBz32SUn3S/qlpM9JOgs4kWTh032SZkraqD17epwo6bb09ssl/UdaxO2OilW1w/nmoO+/ErgxIrrruZakv5T08Yr769ICfkj6I0k/T2P+SiOWX7fm5ERh00UnSZ3/h4Ftkk4AkHRG+tgrIuKlwN9HxHeAu4F3RsSxEbGzxnV/Dbw2Io4D/hz4uxHiuBE4XtK89P7ZJMljNNfqJ+n3gD8AXh0RxwIl4J31Pt+sFpfwsOliFUmBOEj2hlgF3AOcDnytXE8qIn63l9fdD1gjaSnJvh8ttU6OiN2SrgfOkvRdklLeN47mWoOcBpwA/Gda8mQmjVu8z5qME4VNeZL2J9m85pi0RHMeCEl/theXKbKnBT6j4vjfALdGxMq0C+i2Oq71TeB/AALWRkTvXlyrMo7KWASsiYgL6/j+ZnvFXU82HZwFfCMiDksr6h4CbABeS7Ll6nsktUN/UgHoAmZXXGMjySd2gP9acXw/9pStPrfOeG4jqXp7Pnu6neq91kaSPQtQst/5kvT4LSStlAXpY/tLOqzOeMxqcqKw6WAVcO2gY98FVkXED0nKON+dVkwtDxRfCVxWHswG/gr4kqS7Sfr/y/4e+J+S7qXOFnpE9JHsNTAPuH0vr/VdYH9JDwAfItlPnIh4EPjvwI8k/YokATbcFrfWnFw91szManKLwszManKiMDOzmpwozMysJicKMzOryYnCzMxqcqIwM7OanCjMzKwmJwozM6vp/wMjXFudR8XVYQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "data = {'actual': actual, 'residual': residual}\n",
    "data_frame = pd.DataFrame(data)\n",
    "\n",
    "plot = data_frame.plot.scatter(\n",
    "    x='actual',\n",
    "    y='residual',\n",
    "    legend=False,\n",
    "    color=dr_light_blue,\n",
    ")\n",
    "plot.set_facecolor(dr_dark_blue)\n",
    "\n",
    "# define our axes with a minuscule bit of padding\n",
    "min_x = min(data['actual']) - 5\n",
    "max_x = max(data['actual']) + 5\n",
    "min_y = min(data['residual']) - 5\n",
    "max_y = max(data['residual']) + 5\n",
    "\n",
    "plt.xlabel('Actual Value')\n",
    "plt.ylabel('Residual Value')\n",
    "plt.axis('equal')\n",
    "plt.xlim(min_x, max_x)\n",
    "plt.ylim(min_y, max_y)\n",
    "\n",
    "plt.title('Residual Values vs. Actual Values', y=1.04)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this dataset, these charts indicate that the model tends to under-predict blowouts: games which were won by 20+ points were predicted to be much closer."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
